function createBody() {
    function createElement(tag, id, style, data) {
        const ele = document.createElement(tag);
        if (id)
            ele.setAttribute('id', id)
        if (style)
            ele.setAttribute('style', style);
        if (data)
            ele.innerHTML = data;
        return ele;
    }

    document.title = 'Shopping Cart';
    const mainContainer = createElement("div", "main-container");
    const addItemsDiv = createElement("div", "addItemsDiv");
    const addItemsForm = createElement("form", "addItemsForm");
    const pageTitle = createElement("h2", '', '', 'Shopping Cart');
    addItemsForm.append(pageTitle);
    const label = createElement("label", '', '', "Select a User:");
    label.setAttribute("for", "users");
    addItemsForm.append(label);

    const userList = createElement("select", "userList");
    userList.setAttribute("name", "user");
    const defaultOption = createElement('option', '', '', 'Select A User');
    defaultOption.setAttribute('value', 'Select a User');
    const userA = createElement('option', '', '', 'Varun');
    userA.setAttribute('value', 'Varun');
    const userB = createElement('option', '', '', 'Ashish');
    userB.setAttribute('value', 'Ashish');
    userList.append(defaultOption);
    userList.append(userA);
    userList.append(userB);
    addItemsForm.append(userList);
    addItemsForm.append(createElement("br"));

    const itemName = createElement("input", "itemName");
    itemName.setAttribute("type", "text");
    addItemsForm.append(itemName);
    const addToCartButton = createElement('button', 'addToCartButton', '', 'Add to Cart');
    addItemsForm.appendChild(addToCartButton);
    //Form Created and now adding it to parent div addItemsDiv
    addItemsDiv.append(addItemsForm);

    //-------------------------------
    const displaySection = createElement("div", "displaySection");
    const userCartItems = createElement('div', 'userCartItems');
    displaySection.append(userCartItems);
    const userPurchasedItems = createElement('div', 'userPurchasedItems');
    displaySection.append(userPurchasedItems);

    mainContainer.append(addItemsDiv);
    mainContainer.append(displaySection);
    document.body.append(mainContainer);
}
createBody();


// DOM Operations using Closures
function parent() {
    let obj = {
        'Varun': [],
        'Ashish': []
    };
    let purchasedItemsObj = {
        'Varun': [],
        'Ashish': []
    };

    function addItem(user, item) {
        if (!obj[user]) {
            alert('Please select a User.');
            itemName.value = '';
        }
        else if (!item)
            alert('Please enter a valid item');
        else
            obj[user].push(item);
    }

    function removeItem(user, item) {
        const index = obj[user].indexOf(item);
        obj[user].splice(index, 1);
    }

    function displayCartItems(user) {
        if (obj[user]) {
            userCartItems.innerHTML = '<h3>Cart Items:</h3>';
            obj[user].forEach((item) => {
                userCartItems.innerHTML += `
        <div id="${item}" style="background-color:white;">
            <p>${item}</p>
            <button id = "purchase">Purchase</button>
            <button id = "delete">Delete</button>
        </div>`
            })
        }
    }
    function purchaseItem(user, item) {
        if (obj[user])
            purchasedItemsObj[user].push(item);

    }
    function removePurchasedItem(user, item) {
        const index = purchasedItemsObj[user].indexOf(item);
        purchasedItemsObj[user].splice(index, 1);

    }
    function displayPurchasedItems(user) {
        if (obj[user]) {
            userPurchasedItems.innerHTML = '<h3>Purchased Items:</h3>';
            purchasedItemsObj[user].forEach((item) => {
                userPurchasedItems.innerHTML += `<div id ="${item}" style="background-color:white;">
                <p>${item}</p>
                <button id = "deletePurchase">Delete Purchase</button>
                </div>`
            })
        }
    }
    return { obj, purchasedItemsObj, addItem, removeItem, displayCartItems, purchaseItem, removePurchasedItem, displayPurchasedItems };
}

const { obj, purchasedItemsObj, addItem, removeItem, displayCartItems, purchaseItem, removePurchasedItem, displayPurchasedItems } = parent();


const itemName = document.querySelector('#itemName');
const button = document.querySelector('#addToCartButton');
const userList = document.querySelector('#userList');
const displaySection = document.querySelector('#displaySection');
const userCartItems = document.querySelector('#userCartItems');
const userPurchasedItems = document.querySelector('#userPurchasedItems');

// Adding Cart Item
button.addEventListener('click', function (e) {
    e.preventDefault(); //prevents console refresh
    addItem(userList.value, itemName.value);
    itemName.value = '';
    console.log(obj);
    displayCartItems(userList.value);
    e.stopImmediatePropagation();
});

userList.addEventListener('change', function (e) {
    e.preventDefault(); //prevents console refresh
    displayCartItems(userList.value);
    displayPurchasedItems(userList.value);
    e.stopImmediatePropagation();
})

userCartItems.addEventListener('click', function (e) {
    let event = e.target;
    if (event.id == 'delete') {
        removeItem(userList.value, event.parentNode.id);
        displayCartItems(userList.value);
    }
    if (event.id == 'purchase') {
        purchaseItem(userList.value, event.parentNode.id);
        removeItem(userList.value, event.parentNode.id);
        displayPurchasedItems(userList.value);
        displayCartItems(userList.value);
    }
    e.stopImmediatePropagation();
})

userPurchasedItems.addEventListener('click', function (e) {
    let event = e.target;
    if (event.id == 'deletePurchase') {
        removePurchasedItem(userList.value, event.parentNode.id);
        displayPurchasedItems(userList.value);
    }
    e.stopImmediatePropagation();
})